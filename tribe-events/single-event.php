<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$event_id = get_the_ID();
$interest = get_field('interest_only');
?>
<?php if (has_post_thumbnail( $post->ID ) ){
    $thumb_id = get_post_thumbnail_id();
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
    $thumb_url = $thumb_url_array[0];
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

    <div class="feat_image" style="background-image: url('<?php echo $thumb_url;?>');">
      <!-- <div class="jumbotron page_header"> -->
       <!-- <div class="container-fluid"> -->
          <!-- <h1><?php the_title();?></h1> -->
        <!-- </div> -->
      <!-- </div> -->
    </div>

<?php } else { ?>
    <!-- <div class="jumbotron page_header"> -->
      <!-- <div class="container-fluid"> -->
        <!-- <h1><?php the_title();?></h1> -->
      <!-- </div> -->
    <!-- </div> -->
<?php } ?>

<div class="container-fluid">
  <section class="row page_content">


		<p class="tribe-events-back">
			<a href="<?php echo tribe_get_events_link() ?>"> <?php _e( '&laquo; All Workshops', 'tribe-events-calendar' ) ?></a>
		</p>
		<section class="single-event-content">
			<article class="row">
				<!-- Notices -->

				<?phpif(!$interest) tribe_events_the_notices(); ?>

				<?php the_title( '<h2 class="col-sm-12 tribe-events-single-event-title summary entry-title">', '</h2>' ); ?>

				<?php if(!$interest){ ?>
					<div class="col-sm-12 tribe-events-schedule updated published tribe-clearfix">
						<?php echo tribe_events_event_schedule_details( $event_id, '<h3>', '</h3>' ); ?>
						<?php if ( tribe_get_cost() ) : ?>
							<span class="tribe-events-divider">|</span>
							<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span>
						<?php endif; ?>
					</div>
				<?php } ?>



				<div class="clear"></div>
				<div class="col-sm-8">
					<!-- Event header -->
					<div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>
						<!-- Navigation -->
						<h3 class="tribe-events-visuallyhidden"><?php _e( 'Event Navigation', 'tribe-events-calendar' ) ?></h3>
						<!-- <ul class="tribe-events-sub-nav">
							<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
							<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
						</ul> -->
						<!-- .tribe-events-sub-nav -->
					</div>
					<!-- #tribe-events-header -->

					<?php while ( have_posts() ) :  the_post(); ?>
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<!-- Event featured image, but exclude link -->
							<?php //echo tribe_event_featured_image( $event_id, 'full', false ); ?>

							<!-- Event content -->
							<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
							<div class="tribe-events-single-event-description tribe-events-content entry-content description">

								<?php if (get_field('tab_1_title') ){ ?>
									<div class="tab-section">
										<ul class="nav nav-tabs">
											<li class="active"><a href="#"><?php the_field('tab_1_title'); ?></a></li>
											<?php if (get_field('tab_2_title')){ ?><li><a href="#"><?php the_field('tab_2_title'); ?></a></li><?php } ?>
											<?php if (get_field('tab_3_title')){ ?><li><a href="#"><?php the_field('tab_3_title'); ?></a></li><?php } ?>
											<?php if (get_field('tab_4_title')){ ?><li><a href="#"><?php the_field('tab_4_title'); ?></a></li><?php } ?>
										</ul>
										<div class="tab-content" id="tab1">
											<?php the_content(); ?>
										</div>
										<div class="tab-content" id="tab2">
											<?php the_field('tab_2_content'); ?>
										</div>
										<div class="tab-content" id="tab3">
											<?php the_field('tab_3_content'); ?>
										</div>
										<div class="tab-content" id="tab4">
											<?php the_field('tab_4_content'); ?>
										</div>
								</div>
								<?php } ?>
							</div>

						</div> <!-- #post-x -->
						<?php if ( get_post_type() == TribeEvents::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
					<?php endwhile; ?>
				</div>

				<div class="col-sm-4 rightcol">
					<div class="tribe-register">
						<?php echo do_shortcode('[contact-form-7 id="287" title="Enrollment"]'); ?>
					</div>

					<!-- .tribe-events-single-event-description -->
					<?php // do_action( 'tribe_events_single_event_after_the_content' ) ?>

					<!-- Event meta -->
					<?php if(!$interest){ ?>
						<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
						<?php
						/**
						 * The tribe_events_single_event_meta() function has been deprecated and has been
						 * left in place only to help customers with existing meta factory customizations
						 * to transition: if you are one of those users, please review the new meta templates
						 * and make the switch!
						 */
						if ( ! apply_filters( 'tribe_events_single_event_meta_legacy_mode', false ) ) {
							tribe_get_template_part( 'modules/meta' );
						} else {
							echo tribe_events_single_event_meta();
						}
						?>
						<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
					<?php } ?>
					<?php if(get_field('sidebar_details')){ ?>
						<div class="sidebar-details">
							<?php $sidebar_details = get_field('sidebar_details');
							$sidebar_details = str_replace("*", '<i class="fa fa-star"></i></i>', $sidebar_details);
							echo $sidebar_details; ?>
						</div>
					<?php } ?>

				</div>

				<!-- Event footer -->
				<div id="tribe-events-footer">
					<!-- Navigation -->
					<!-- Navigation -->
					<h3 class="tribe-events-visuallyhidden"><?php _e( 'Event Navigation', 'tribe-events-calendar' ) ?></h3>
					<ul class="tribe-events-sub-nav">
						<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
						<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
					</ul>
					<!-- .tribe-events-sub-nav -->
				</div>
				<!-- #tribe-events-footer -->

			</article>
		</section>
	</section>
</div>

<?php
$images = get_field('gallery');

if( $images ): ?>
	<div class="event-gallery">
		<?php foreach( $images as $image ): ?>
		  <a class="gallery-item" href="">
				<img src="<?php echo $image['sizes']['medium']; ?>" data-w="<?php echo $image['sizes']['large-width']; ?>" data-h="<?php echo $image['sizes']['large-height']; ?>" data-caption="<?php echo $image['caption']; ?>" data-hq="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
		  </a>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
