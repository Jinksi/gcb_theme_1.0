<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php if (has_post_thumbnail( $post->ID ) ){
    $thumb_id = get_post_thumbnail_id();
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
    $thumb_url = $thumb_url_array[0];
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

    <div class="feat_image" style="background-image: url('<?php echo $thumb_url;?>');"></div>

<?php } else {}?>

<div class="container-fluid">

    <section class="row page_content">
        <article class="col-xs-12 col-sm-8">

            <h1><?php the_title();?></h1>
            <?php the_content(); ?>

        </article>
    </section>

    <aside class="col-xs-12 col-sm-8">
        <?php get_sidebar(); ?>
    </aside>

    <?php endwhile; else: ?>
        <p><?php _e('Sorry, this page does not exist.'); ?></p>
    <?php endif; ?>

</div>

<?php get_footer(); ?>
