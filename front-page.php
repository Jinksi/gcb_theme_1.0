<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php if (has_post_thumbnail( $post->ID ) ){
    $thumb_id = get_post_thumbnail_id();
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
	$thumb_url = $thumb_url_array[0];
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

    <div class="feat_image frontpage" style="background-image: url('<?php echo $thumb_url;?>');">
      <div class="jumbotron main_header">
       <div class="container">
          <a href="/" class="logo">Gold Coast Botany</a>
          <p class="blurb" ><?php the_field('blurb'); ?></p>
        </div>
      </div>
    </div>

<?php } else { ?>
    <section class="jumbotron main_header">
        <div class="container">
            <a href="/" class="logo">Gold Coast Botany</a>
            <p class="blurb" ><?php the_field('blurb'); ?></p>
        </div>
    </section>

<?php }; ?>
<div class="">

  <div class="container-fluid">
      <div class="row main_content">
        <section class="col-sm-4 card">
            <?php the_field('section_1'); ?>
        </section>
        <section class="col-sm-4 card">
            <?php the_field('section_2'); ?>
        </section>
        <section class="col-sm-4 card">
            <?php the_field('section_3'); ?>

        </section>
      </div>
  </div>
</div>
<?php endwhile; else: ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
