<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php wp_title( '');?>
    </title>
    <!-- Bootstrap -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/_bootstrap.css" rel="stylesheet">
    <!-- Main -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/gcb_pre.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/gcb_main.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/pswp/photoswipe.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/pswp/default-skin/default-skin.css">


    <!-- icons & favicons -->
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/img/apple-icon-touch.png">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
    <!--[if IE]>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <![endif]-->
    <!-- or, set /favicon.ico for IE10 win -->
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/win8-tile-icon.png">
    <link href="https://opensource.keycdn.com/fontawesome/4.6.3/font-awesome.min.css" rel="stylesheet">
    <!-- TYPEKIT -->
    <script src="//use.typekit.net/lxt5rpn.js"></script>
    <script>
        try {
            Typekit.load();
        } catch (e) {}
    </script>
    <?php wp_head(); ?>
</head>

<body <?php // if ( is_admin_bar_showing() ) echo 'style="padding-top:58px;"'; ?> >

    <!-- NAV -->

    <nav class="navbar navbar-default main_nav" <?php //if ( is_admin_bar_showing() ) echo 'style="top:30px;"'; ?> >


        <div class="container-fluid">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav_main">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/GCB_logo.png"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="nav_main">
                <!-- <ul class="nav navbar-nav navbar-right"> -->
                <?php wp_nav_menu( array( 'container'=> false , 'theme_location' => 'header-menu', 'items_wrap' => '
                <ul id="%1$s" class="nav navbar-nav navbar-right">%3$s</ul>') ); ?>

                <!-- <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
          <li><a href="#">Consulting</a></li>
          <li><a href="#">Education</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Contact Us</a></li>    -->
                <!-- </ul> -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
<?php include('inc-edit.php'); ?>
