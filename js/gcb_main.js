$(document).ready( function(){

  $(function() {
      //FastClick.attach(document.body);
  });
  $('.tab-content').hide();
  $('.tab-content#tab1').show();

  $('.tab-section .nav-tabs li a').click(function(e){
    e.preventDefault();
    var tabIndex = $(this).parent('li').index() + 1;
    $('.tab-section .nav-tabs li').removeClass('active');
    $(this).parent('li').addClass('active');
    console.log(tabIndex);
    $('.tab-content').hide();
    $('.tab-content#tab' + tabIndex).show();
  });

  $('.event-gallery').flickity({
    // options
    imagesLoaded: true,
    contain: true,
    prevNextButtons: false,
    pageDots: false,
    autoPlay: 5000,
    initialIndex: 1
  });
  var items = [];

  $('.event-gallery img').each(function(){
    var hq = $(this).attr('data-hq');
    var height = $(this).attr('data-h');
    var width = $(this).attr('data-w');
    var caption = $(this).attr('data-caption');
    var item = {
      src: hq,
      w: width,
      h: height,
      title: caption
    };
    items.push(item);
  });

  var pswpElement = document.querySelectorAll('.pswp')[0];
  $('.event-gallery a').click(function(e){
    e.preventDefault();
    var index = $(this).index();

    // define options (if needed)
    var options = {
        // optionName: 'option value'
        // for example:
        index: index, // start at first slide
        shareEl: true
    };
    // Initializes and opens PhotoSwipe
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
  });

});
