<?php add_theme_support( 'post-thumbnails' );

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	)
);


function register_my_menus() {
	register_nav_menus(
		array(
		'header-menu' => __( 'Header Menu' ),
		'footer-menu' => __( 'Footer Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/////////////////////////////////////
add_image_size( 'feature_image', 950, 400, true );

// add_action('acf/input/admin_head', 'my_acf_admin_head');

function my_acf_admin_head() {

    ?>
    <script type="text/javascript">
    (function($) {

        $(document).ready(function(){

            $('.acf-field-55eb7c5e8efff	 .acf-input').append( $('#postdivrich') );

        });

    })(jQuery);
    </script>
    <style type="text/css">
        .acf-field #wp-content-editor-tools {
            background: transparent;
            padding-top: 0;
        }
    </style>
    <?php

}

///////////////////////////////////////////////////////
//Remove admin tool bar
function my_function_admin_bar(){
    return 0;
}
add_filter( 'show_admin_bar' , 'my_function_admin_bar');

?>
