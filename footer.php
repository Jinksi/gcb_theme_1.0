<!-- FOOTER -->

<footer class="jumbotron main_footer">
    <div class="container-fluid">
         <div class="row">

            <div class="col-xs-12 col-sm-6">
                <h3>Contact Us</h3>
                <p>Email: <a href="mailto:info@goldcoastbotany.com.au">info@goldcoastbotany.com.au</a></p>
                <p>Phone: <a href="tel:+61411694258">0411 694 258</a></p>
                <p>Post: P.O. Box 471, Mudgeeraba QLD 4213</p>
            </div>

            <div class="col-xs-12 col-sm-6">
                <h3>Navigation</h3>
                <?php wp_nav_menu( array( 'container' => false , 'theme_location' => 'footer-menu', 'items_wrap'  => '<ul id="%1$s" class="nav nav-stacked footer_nav">%3$s</ul>') );  ?>
                <!-- <ul class="nav nav-stacked footer_nav"> -->
                <?php // wp_list_pages(array('title_li' => '')); ?>
                </ul>
             </div>

        </div>

        <p class="footer_copyright">
        <a class="footer_logo" alt="Gold Coast Botany" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/GCB_logo.png"></a><br/>
        &copy; <?php echo date('Y'); ?> <a href="http://goldcoastbotany.com.au" alt="Gold Coast Botany">Gold Coast Botany</a></p>
    </div>
</footer>

<?php include('pswp.php'); ?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
<!-- <script src="<?php// echo get_template_directory_uri(); ?>/js/fastclick.js"></script> -->
<script src="<?php echo get_template_directory_uri(); ?>/pswp/photoswipe.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/pswp/photoswipe-ui-default.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/flickity/1.1.1/flickity.pkgd.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/gcb_main.js"></script>
<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-60192834-1', 'auto');
ga('send', 'pageview');

</script>

<?php wp_footer(); ?>

</body>
</html>
